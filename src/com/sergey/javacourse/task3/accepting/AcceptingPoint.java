package com.sergey.javacourse.task3.accepting;

import com.sergey.javacourse.task3.Package;
import com.sergey.javacourse.task3.readers.FilePackageReader;
import com.sergey.javacourse.task3.resources.PlacesReadFrom;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AcceptingPoint {
    private static final int PACKAGE_PARAMETERS = 3;
    private static final Logger LOGGER =
            Logger.getLogger(AcceptingPoint.class.getName());
    private int goodPackages = 0;
    private ArrayList<Package> tmpStorage;

    public AcceptingPoint(PlacesReadFrom type, String fileName) {
        switch (type) {
            case READ_FROM_FILE:
            {
                FilePackageReader reader = new FilePackageReader();
                ArrayList<String> packagesData =
                        new ArrayList<>(reader.readPackages(fileName));

                int line = 0;
                tmpStorage = new ArrayList<>();
                for (String v: packagesData) {
                    String[] packageParams = v.split(";");
                    line++;

                    if (packageParams.length != PACKAGE_PARAMETERS){
                        LOGGER.log(Level.WARNING,"Cannot get all information about package. File "+fileName+" at line "+line+".\n");
                    }
                    else {
                        Package pack = new Package(
                                Integer.parseInt(packageParams[0]),
                                Integer.parseInt(packageParams[1]),
                                Integer.parseInt(packageParams[2]));

                        tmpStorage.add(pack);
                        goodPackages++;
                    }
                }
                break;
            }
            default:
            {
                LOGGER.log(Level.WARNING, "Unexpected place for reading packages.");
            }
        }
    }

    public Package getPackage() {
        goodPackages--;
        return tmpStorage.get(goodPackages);
    }

    public boolean isEmpty() {
        if (goodPackages == 0) {
            return true;
        } else {
            return false;
        }
    }
}
