package com.sergey.javacourse.task3.storage;

import com.sergey.javacourse.task3.Package;
import com.sergey.javacourse.task3.accepting.AcceptingPoint;
import com.sergey.javacourse.task3.exceptions.PackageDataException;
import com.sergey.javacourse.task3.exceptions.PackageSizeException;
import com.sergey.javacourse.task3.sorting.SortingTape;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Storage implements SortingTape {
    private static final Logger LOGGER =
            Logger.getLogger(AcceptingPoint.class.getName());
    private ArrayList<Package> storage;
    private int size;
    private int amountOfPackages;

    public Storage() {
        size = 10;
        amountOfPackages = 0;
        storage = new ArrayList<>();
    }

    public Storage(int size) {
        amountOfPackages = 0;
        this.size = size;
        storage = new ArrayList<>();
    }

    public ArrayList<Package> getStorage() {
        return storage;
    }

    public int getSize() {
        return size;
    }

    private void addToStorage(Package tmpPackage) {
        storage.add(tmpPackage);
    }

    private void incAmountOfPackages() {
        amountOfPackages++;
    }

    public void addPackage(Package tmpPackage) {
        addToStorage(tmpPackage);
        incAmountOfPackages();
    }

    @Override
    public void sort(Package acceptedPackage) {
        int packageId = acceptedPackage.getPackageID();
        int placeId = acceptedPackage.getPlaceID();
        int packageSize = acceptedPackage.getPackageSize();

        if ((placeId < 0) || (placeId > size))
            throw new PackageDataException("Package "
                    +packageId+" placeID out of bounds.", acceptedPackage);

        if (packageSize > size)
            throw new PackageSizeException("Size error at package "
                    +packageId+". Not enough space.", acceptedPackage);

        for (Package storagePackage: storage) {
            if (storagePackage.getPlaceID() == placeId)
                throw new PackageDataException("PlaceID error at package "
                        +packageId+". Cannot put into storage on "
                        +placeId+" place.", acceptedPackage);
        }

        addPackage(acceptedPackage);
    }

    public void printLog(String place) {
        if (amountOfPackages != 0) {
            LOGGER.log(Level.FINEST, place);

            for (Package v : storage) {
                LOGGER.log(Level.FINEST, "Package: " + v.toString());
            }
        }
    }
}
