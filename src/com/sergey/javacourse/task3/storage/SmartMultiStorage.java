package com.sergey.javacourse.task3.storage;

import com.sergey.javacourse.task3.Package;
import com.sergey.javacourse.task3.sorting.SmartMultiSortingTape;

public class SmartMultiStorage extends MultiStorage implements SmartMultiSortingTape {

    public SmartMultiStorage() {
        super();
    }

    public SmartMultiStorage(int size) {
        super(size);
    }

    @Override
    public void smartSort(Package acceptedPackage, MultiStorage standardStorage) {
        int packageId = acceptedPackage.getPackageID();
        int placeId = acceptedPackage.getPlaceID();
        int packageSize = acceptedPackage.getPackageSize();

        if ((placeId < 0) || (placeId > getSize())) {
            standardStorage.sort(acceptedPackage);
            return;
        }

        int cellFreeSpace = getCellFreeSpace(placeId);
        if (cellFreeSpace < packageSize) {
            standardStorage.sort(acceptedPackage);
            return;
        }

        addToStorage(acceptedPackage, placeId, cellFreeSpace, packageSize);
    }
}
