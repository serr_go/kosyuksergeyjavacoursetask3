package com.sergey.javacourse.task3.storage;

import com.sergey.javacourse.task3.Package;
import com.sergey.javacourse.task3.accepting.AcceptingPoint;
import com.sergey.javacourse.task3.exceptions.PackageDataException;
import com.sergey.javacourse.task3.exceptions.PackageSizeException;
import com.sergey.javacourse.task3.sorting.SortingTape;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MultiStorage implements SortingTape {
    private static final Logger LOGGER =
            Logger.getLogger(AcceptingPoint.class.getName());
    private Map<Integer,ArrayList<Package>> storage;
    private Integer[] cellSpaces;
    private int size;
    private int amountOfPackages;

    public MultiStorage() {
        size = 10;
        amountOfPackages = 0;
        storage = new HashMap<>();
        cellSpaces = new Integer[size+1];

        for(int i = 1; i <= size; i++) {
            storage.put(i,new ArrayList<>());
            cellSpaces[i] = i;
        }
    }

    public MultiStorage(int size) {
        amountOfPackages = 0;
        this.size = size;
        storage = new HashMap<>();
        cellSpaces = new Integer[size+1];

        for(int i = 1; i <= this.size; i++) {
            storage.put(i,new ArrayList<>());
            cellSpaces[i] = i;
        }
    }

    public Map<Integer,ArrayList<Package>> getStorage() {
        return storage;
    }

    public int getSize() {
        return size;
    }

    public int getCellFreeSpace(int placeId) {
        return cellSpaces[placeId];
    }

    public void addToStorage(Package acceptedPackage, int placeId, int cellFreeSpace, int packageSize) {
        storage.get(placeId).add(acceptedPackage);
        amountOfPackages++;
        cellSpaces[placeId] = cellFreeSpace - packageSize;
    }

    @Override
    public void sort(Package acceptedPackage) {
        int packageId = acceptedPackage.getPackageID();
        int placeId = acceptedPackage.getPlaceID();
        int packageSize = acceptedPackage.getPackageSize();

        if ((placeId < 0) || (placeId > size))
            throw new PackageDataException("Package "
                    +packageId+" placeID out of bounds.", acceptedPackage);

        int cellFreeSpace = cellSpaces[placeId];
        if (cellFreeSpace < packageSize)
            throw new PackageSizeException("Size error at package "
                    +packageId+". Not enough space.", acceptedPackage);

        addToStorage(acceptedPackage, placeId, cellFreeSpace, packageSize);
    }

    public void printLog(String place) {
        if (amountOfPackages != 0) {
            LOGGER.log(Level.FINEST, place);

            storage.forEach((k,v) ->{
                if(!v.isEmpty())
                    LOGGER.log(Level.FINEST, "Cell: "+ k + v.toString());
            });
        }
    }
}
