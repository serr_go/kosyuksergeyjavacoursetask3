package com.sergey.javacourse.task3.storage;

import com.sergey.javacourse.task3.Package;
import com.sergey.javacourse.task3.sorting.SmartSortingTape;

public class SmartStorage extends Storage implements SmartSortingTape {

    public SmartStorage() {
        super();
    }

    public SmartStorage(int size) {
        super(size);
    }

    @Override
    public void smartSort(Package acceptedPackage, Storage standardStorage) {
        int packageId = acceptedPackage.getPackageID();
        int placeId = acceptedPackage.getPlaceID();
        int packageSize = acceptedPackage.getPackageSize();

        if (placeId < 0 || placeId > getSize()){
            standardStorage.sort(acceptedPackage);
            return;
        }

        if (packageSize > getSize()){
            standardStorage.sort(acceptedPackage);
            return;
        }


        for (Package storagePackage: getStorage()) {
            if (storagePackage.getPlaceID() == placeId) {
                standardStorage.sort(acceptedPackage);
                return;
            }
        }

        addPackage(acceptedPackage);
    }
}
