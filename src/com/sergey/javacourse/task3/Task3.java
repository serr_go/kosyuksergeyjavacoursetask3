package com.sergey.javacourse.task3;

import com.sergey.javacourse.task3.accepting.AcceptingPoint;
import com.sergey.javacourse.task3.tasks.Task3_1;
import com.sergey.javacourse.task3.tasks.Task3_2;
import com.sergey.javacourse.task3.tasks.Task3_3;
import com.sergey.javacourse.task3.tasks.Task3_5;

import java.io.IOException;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import static com.sergey.javacourse.task3.resources.PlacesReadFrom.*;

public class Task3 {
    private static final Logger LOGGER =
            Logger.getLogger(AcceptingPoint.class.getName());
    private static final String FILE_NAME = "packages.csv";


    public static void main(String[] args) {
        try {
            LogManager.getLogManager().readConfiguration(
                    Task3.class.getResourceAsStream("/com/sergey/javacourse/task3/resources/logging.properties"));
        } catch (IOException e) {
            System.err.println("Could not setup logger configuration: " + e.toString());
        }

        AcceptingPoint acceptingPoint = new AcceptingPoint(READ_FROM_FILE, FILE_NAME);

        //Task3_1 firstTask = new Task3_1(acceptingPoint);
        //firstTask.work();

        //Task3_2 secondTask = new Task3_2(acceptingPoint);
        //secondTask.work();

        //Task3_3 thirdTask = new Task3_3(acceptingPoint);
        //thirdTask.work();

        Task3_5 fiveTask = new Task3_5(acceptingPoint);
        fiveTask.work();
    }
}
