package com.sergey.javacourse.task3.exceptions;

import com.sergey.javacourse.task3.Package;

public class PackageSizeException extends RuntimeException{
    private Package acceptedPackage;

    public PackageSizeException(String message, Package acceptedPackage){
        super(message);
        this.acceptedPackage = acceptedPackage;
    }

    public Package returnPackage() {
        return acceptedPackage;
    }
}
