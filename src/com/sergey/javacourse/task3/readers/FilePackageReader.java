package com.sergey.javacourse.task3.readers;

import com.sergey.javacourse.task3.Package;
import com.sergey.javacourse.task3.accepting.AcceptingPoint;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FilePackageReader implements PackageReader{
    private static final Logger LOGGER =
            Logger.getLogger(AcceptingPoint.class.getName());

    @Override
    public ArrayList<String> readPackages(String fileName) {
        ArrayList<String> packagesData = null;
        try(FileReader file =
                    new FileReader("src/com/sergey/javacourse/task3/resources/"+fileName)){

            Scanner scan = new Scanner(file);
            packagesData = new ArrayList<>();
            while (scan.hasNextLine()) {
                packagesData.add(scan.nextLine());
            }
        } catch (FileNotFoundException e) {
            LOGGER.log(Level.WARNING, "Unexpected exception.", e);
            System.exit(1);
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Unexpected exception.", e);
        }
        return packagesData;
    }
}
