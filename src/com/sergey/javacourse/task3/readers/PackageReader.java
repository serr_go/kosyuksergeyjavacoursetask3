package com.sergey.javacourse.task3.readers;

import java.util.ArrayList;

public interface PackageReader {
    public ArrayList<String> readPackages(String fileName);
}
