package com.sergey.javacourse.task3.tasks;

import com.sergey.javacourse.task3.Package;
import com.sergey.javacourse.task3.accepting.AcceptingPoint;
import com.sergey.javacourse.task3.exceptions.PackageDataException;
import com.sergey.javacourse.task3.storage.MultiStorage;
import com.sergey.javacourse.task3.storage.SmartMultiStorage;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Task3_3 extends Task {
    private static final Logger LOGGER =
            Logger.getLogger(AcceptingPoint.class.getName());

    public Task3_3(AcceptingPoint acceptingPoint) {
        super(acceptingPoint);
    }

    @Override
    public void work() {
        SmartMultiStorage storageOne = new SmartMultiStorage();
        MultiStorage storageTwo = new MultiStorage();
        ArrayList<Package> trashList = new ArrayList<>();

        while (!getAcceptingPoint().isEmpty()) {
            try {
                storageOne.smartSort(getAcceptingPoint().getPackage(), storageTwo);
            } catch (PackageDataException e) {
                LOGGER.log(Level.WARNING, "Unexpected exception", e);
                trashList.add(e.returnPackage());
            }
        }

        if (!trashList.isEmpty()) {
            LOGGER.log(Level.FINEST, "Trash list:");
            for (Package trash : trashList) {
                LOGGER.log(Level.FINEST, "Package: " + trash.toString());
            }
        }

        storageOne.printLog("First storage.");
        storageTwo.printLog("Second storage.");
    }
}
