package com.sergey.javacourse.task3.tasks;

import com.sergey.javacourse.task3.Package;
import com.sergey.javacourse.task3.accepting.AcceptingPoint;
import com.sergey.javacourse.task3.exceptions.PackageDataException;
import com.sergey.javacourse.task3.storage.SmartStorage;
import com.sergey.javacourse.task3.storage.Storage;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Task3_2 extends Task {
    private static final Logger LOGGER =
            Logger.getLogger(AcceptingPoint.class.getName());

    public Task3_2(AcceptingPoint acceptingPoint) {
        super(acceptingPoint);
    }


    @Override
    public void work() {
        SmartStorage storageOne = new SmartStorage();
        Storage storageTwo = new Storage(20);
        ArrayList<Package> trashList = new ArrayList<>();

        while (!getAcceptingPoint().isEmpty()) {
            try {
                storageOne.smartSort(getAcceptingPoint().getPackage(),storageTwo);
            } catch (PackageDataException e) {
                LOGGER.log(Level.WARNING, "Unexpected exception", e);
                trashList.add(e.returnPackage());
            }
        }


        if (!trashList.isEmpty()) {
            LOGGER.log(Level.FINEST, "Trash list:");
            for (Package trash : trashList) {
                LOGGER.log(Level.FINEST, "Package: " + trash.toString());
            }
        }

        storageOne.printLog("First storage.");
        storageTwo.printLog("Second storage.");
    }
}
