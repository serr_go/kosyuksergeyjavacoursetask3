package com.sergey.javacourse.task3.tasks;

import com.sergey.javacourse.task3.accepting.AcceptingPoint;

public abstract class Task {
    private AcceptingPoint acceptingPoint;

    public Task(AcceptingPoint acceptingPoint) {
        this.acceptingPoint = acceptingPoint;
    }

    public AcceptingPoint getAcceptingPoint() {
        return acceptingPoint;
    }

    public abstract void work();
}
