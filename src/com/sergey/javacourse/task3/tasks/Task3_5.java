package com.sergey.javacourse.task3.tasks;

import com.sergey.javacourse.task3.Package;
import com.sergey.javacourse.task3.accepting.AcceptingPoint;
import com.sergey.javacourse.task3.exceptions.PackageDataException;
import com.sergey.javacourse.task3.exceptions.PackageSizeException;
import com.sergey.javacourse.task3.storage.MultiStorage;
import com.sergey.javacourse.task3.storage.SmartMultiStorage;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Task3_5 extends Task {
    private static final Logger LOGGER =
            Logger.getLogger(AcceptingPoint.class.getName());
    private static final int AMOUNT_OF_STORAGE = 2;
    private Integer packageSizes[];
    private boolean arrForRecursion[];
    private boolean mark = false;

    //Recursion doesn't work without this const
    private int MAGIC_ONE = 1;

    public Task3_5(AcceptingPoint acceptingPoint)
    {
        super(acceptingPoint);
    }

    private void packagesRedistribution(int cell, SmartMultiStorage storageOne, MultiStorage storageTwo) {
        ArrayList<Package> storageOneCell = new ArrayList<>(storageOne.getStorage().get(cell));

        for (Package v: storageTwo.getStorage().get(cell)) {
            storageOneCell.add(v);
        }

        int amountOfPackages = 0;
        packageSizes = new Integer[storageOneCell.size()+MAGIC_ONE];

        for (Package storageOnePack: storageOneCell) {
            packageSizes[amountOfPackages] = storageOnePack.getPackageSize();
            amountOfPackages++;
        }

        packageSizes[amountOfPackages] = 0;
        arrForRecursion = new boolean[amountOfPackages+1];

        recursion(0,0,amountOfPackages+MAGIC_ONE, cell);

        ArrayList<Package> fit = new ArrayList<>();
        ArrayList<Package> another = new ArrayList<>();
        for (int i = 0; i < amountOfPackages; i++) {
            if (arrForRecursion[i]) {
                fit.add(storageOneCell.get(i));
            }
            else {
                another.add(storageOneCell.get(i));
            }
        }

        /**
         * В fit пакеты упорядочены оптимально суммарной размерностью cell
         * Заменить текущую ячейку cell первого склада на fit
         * Оставшиеся пакеты в another
         * Заменить текушую ячейку cell второго склада на another
         */

    }

    private void recursion(int i, int sum, int amountOfPackages, int cell) {
        if (i >= amountOfPackages) {
            return;
        }
        if (sum == cell && (i == amountOfPackages-1)) {
            mark = true;
        }

        recursion(i + 1, sum, amountOfPackages, cell);
        if (mark) {
            return;
        }
        recursion(i + 1, sum + packageSizes[i], amountOfPackages, cell);
        if (mark) {
            arrForRecursion[i] = true;
        }
    }

    @Override
    public void work() {
        SmartMultiStorage storageOne = new SmartMultiStorage();
        MultiStorage storageTwo = new MultiStorage();
        ArrayList<Package> trashList = new ArrayList<>();

        while (!getAcceptingPoint().isEmpty()) {
            int attempt = 1;
            Package acceptedPackage = getAcceptingPoint().getPackage();

            while (attempt != AMOUNT_OF_STORAGE) {
                attempt++;
                try {
                    storageOne.smartSort(acceptedPackage, storageTwo);
                } catch (PackageDataException e) {
                    LOGGER.log(Level.WARNING, "Unexpected exception", e);
                    trashList.add(e.returnPackage());
                    break;
                } catch (PackageSizeException  e) {
                    int cell = acceptedPackage.getPlaceID();
                    int freeSpace = storageOne.getCellFreeSpace(cell)
                            + storageTwo.getCellFreeSpace(cell);

                    if (attempt == AMOUNT_OF_STORAGE+1) {
                        LOGGER.log(Level.WARNING, "Unexpected exception", e);
                        trashList.add(e.returnPackage());
                        break;
                    } else {
                        packagesRedistribution(cell, storageOne, storageTwo);
                    }
                }
            }
        }

        if (!trashList.isEmpty()) {
            LOGGER.log(Level.FINEST, "Trash list:");
            for (Package trash : trashList) {
                LOGGER.log(Level.FINEST, "Package: " + trash.toString());
            }
        }

        storageOne.printLog("First storage.");
        storageTwo.printLog("Second storage.");
    }
}
