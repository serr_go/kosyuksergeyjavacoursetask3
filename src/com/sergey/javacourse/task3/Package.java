package com.sergey.javacourse.task3;

public class Package {
    private int packageID;
    private int placeID;
    private int packageSize;

    public Package(int packageID, int placeID, int packageSize) {
        this.packageID = packageID;
        this.placeID = placeID;
        this.packageSize = packageSize;
    }

    @Override
    public String toString() {
        return packageID+";"+placeID+";"+packageSize;
    }

    public int getPackageID() {
        return packageID;
    }

    public int getPackageSize() {
        return packageSize;
    }

    public int getPlaceID() {
        return placeID;
    }

    public void setPackageID(int packageID) {
        this.packageID = packageID;
    }

    public void setPackageSize(int packageSize) {
        this.packageSize = packageSize;
    }

    public void setPlaceID(int placeID) {
        this.placeID = placeID;
    }
}
