package com.sergey.javacourse.task3.sorting;

import com.sergey.javacourse.task3.Package;

public interface SortingTape {
    void sort(Package acceptedPackage);
}
