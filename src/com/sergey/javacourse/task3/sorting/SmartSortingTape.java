package com.sergey.javacourse.task3.sorting;

import com.sergey.javacourse.task3.Package;
import com.sergey.javacourse.task3.storage.Storage;

public interface SmartSortingTape {
    void smartSort(Package acceptedPackage, Storage standardStorage);
}
