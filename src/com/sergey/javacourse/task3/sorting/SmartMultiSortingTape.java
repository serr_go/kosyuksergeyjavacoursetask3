package com.sergey.javacourse.task3.sorting;

import com.sergey.javacourse.task3.Package;
import com.sergey.javacourse.task3.storage.MultiStorage;

public interface SmartMultiSortingTape {
    void smartSort(Package acceptedPackage, MultiStorage standardStorage);
}
